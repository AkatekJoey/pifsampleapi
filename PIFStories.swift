//
//  PIFstory.swift
//  PIF
//
//  Created by Louie Je Decatoria on 08/09/2016.
//  Copyright © 2016 Louie Je Decatoria. All rights reserved.
//

import Foundation

class PIFStories: NSObject, NSCoding, ResponseObjectSerializable {
    
    var storyTitle: String?
    var storySummary: String?
    var storyDesc: String?
    var storyImage: String?
    var storyVideo: String?
    var storyCity: String?
    var storyCountry: String?
    var storyDate: String?
    var storyLOI: String?
    var storySlug: String?
    
    var storyViews: String?
    var storyImage_path: String?
    
    override init() {
        self.storyTitle = ""
        self.storySummary = ""
        self.storyDesc = ""
        self.storyImage = ""
        self.storyVideo = ""
        self.storyCity = ""
        self.storyCountry = ""
        self.storyDate = ""
        self.storyLOI = ""
        self.storySlug = ""
        
        self.storyViews = ""
        self.storyImage_path = ""
    }
    
    required init?(data: AnyObject) {
        super.init()
        
        var key = "Title"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyTitle = data.value(forKeyPath: key) as? String
        }
        
        key = "summary"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storySummary = data.value(forKeyPath: key) as? String
        }
        
        key = "content"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyDesc = data.value(forKeyPath: key) as? String
        }
        
        key = "Preview_Image"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyImage = data.value(forKeyPath: key) as? String
        }
        
        key = "Video_Url"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyVideo = data.value(forKeyPath: key) as? String
        }
        
        key = "city"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyCity = data.value(forKeyPath: key) as? String
        }
         
        key = "country"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyCountry = data.value(forKeyPath: key) as? String
        }
        
        key = "created_at"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyDate = data.value(forKeyPath: key) as? String
        }
        
        key = "prop_l_o_d"
        if (isValidKeypath(data: data, keypath: key)) {
            let storyLOI = data.value(forKeyPath: key) as? [AnyObject]
            let loi = storyLOI![0]
            
            if (isValidKeypath(data: loi, keypath: "line_of_donation")) {
                self.storyLOI = loi.value(forKeyPath: "line_of_donation") as? String
            }
            //self.storyLOI = data.value(forKeyPath: key) as? String
        }
        
        key = "slug"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storySlug = data.value(forKeyPath: key) as? String
        }
        
        key = "views"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyViews = data.value(forKeyPath: key) as? String
        }
        
        key = "image_path"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyImage_path = data.value(forKeyPath: key) as? String
        }
    }
    init(storyName: String?, storySummary: String?, storyDesc: String?, storyImage: String?, storyVideo: String?, storyCity: String?, storyCountry: String?, storyDate: String?, storySlug: String?, storyImage_path: String?) {
        self.storyTitle = storyName
        self.storySummary = storySummary
        self.storyDesc = storyDesc
        self.storyImage = storyImage
        self.storyVideo = storyVideo
        self.storyCity = storyCity
        self.storyCountry = storyCountry
        self.storyDate = storyDate
        self.storySlug = storySlug
        
       // self.storySlug = storySlug
        self.storyImage_path = storyImage_path
    }
    // MARK: - NSCoding
    required convenience init(coder aDecoder: NSCoder) {
        let storyName = aDecoder.decodeObject(forKey: "Title") as? String
        let storySummary = aDecoder.decodeObject(forKey: "summary") as? String
        let storyDesc = aDecoder.decodeObject(forKey: "content") as? String
        let storyImage = aDecoder.decodeObject(forKey: "image_path") as? String
        let storyVideo = aDecoder.decodeObject(forKey: "Video_Url") as? String
        let storyCity = aDecoder.decodeObject(forKey: "city") as? String
        let storyCountry = aDecoder.decodeObject(forKey: "country") as? String
        let storyDate = aDecoder.decodeObject(forKey: "created_at") as? String
        let storySlug = aDecoder.decodeObject(forKey: "storySlug") as? String
        let storyImage_path = aDecoder.decodeObject(forKey: "storyImage_path") as? String

        self.init(storyName: storyName, storySummary: storySummary, storyDesc: storyDesc, storyImage: storyImage, storyVideo: storyVideo, storyCity: storyCity, storyCountry: storyCountry, storyDate: storyDate, storySlug: storySlug, storyImage_path: storyImage_path)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.storyTitle, forKey: "Title")
        aCoder.encode(self.storySummary, forKey: "summary")
        aCoder.encode(self.storyDesc, forKey: "content")
        aCoder.encode(self.storyImage, forKey: "image_path")
        aCoder.encode(self.storyVideo, forKey: "Video_Url")
        aCoder.encode(self.storyCity, forKey: "city")
        aCoder.encode(self.storyCountry, forKey: "country")
        aCoder.encode(self.storyDate, forKey: "created_at")
        aCoder.encode(self.storyDate, forKey: "storySlug")
        aCoder.encode(self.storyDate, forKey: "storyImage_path")
    }
}
