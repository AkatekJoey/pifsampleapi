//
//  StoriesManager.swift
//  PIF
//
//  Created by Louie Je Decatoria on 08/09/2016.
//  Copyright © 2016 Louie Je Decatoria. All rights reserved.
//

import Foundation
import RealmSwift


class StoriesManager: NSObject {
    
    var apiRequest: APIRequest?
    var stories = [PIFStories]()
    var fStories = [PIFStories]()
    var realmStories = [RealmStoriesObject]()
    var realmrealmStoriess = [realmrrealmStories]()
    
    static let sharedInstance: StoriesManager = {
        let instance = StoriesManager()
        var onceToken : Int = 0
        return instance
    }()
    
    func addStories() -> [AnyObject] {
        requestStories()
    
        return stories
    }
    
    func requestStories()
    {
        self.apiRequest?.cancel()
        self.apiRequest = APIRequest.execute(route: APIRouter.stories())
        {
            (data, _, _, error) in
            
            if (error != nil)
            {
                DispatchQueue.main.async
                    {
                }
            }
            else
            {
                let article = data?.value(forKeyPath: "articleEO") as! [AnyObject]
                //debugLog(logMessage: "Results: \(results)")
                self.stories.removeAll()
                article.forEach
                    {
                        obj in
                        
                        let theStories = realmrrealmStories(data: obj)
                        self.realmrealmStoriess.append(theStories!)
                        
                        
                        let realm = try!Realm()
                        realm.beginWrite()
                        let newData = RealmStories()
                        newData.storyTitle = (theStories?.storyTitle
                            )!
                        newData.storyId = (theStories?.storyId)!
                        newData.storySummary = (theStories?.storySummary)!
                        newData.storyDesc = (theStories?.storyDesc)!
                        newData.storyImage = (theStories?.storyImage)!
                        newData.storyVideo = (theStories?.storyVideo)!
                        newData.storyCity = (theStories?.storyCity)!
                        newData.storyCountry = (theStories?.storyCountry)!
                        newData.storyDate = (theStories?.storyDate)!
                        newData.storyLOI = (theStories?.storyLOI)!
                        newData.storySlug = (theStories?.storySlug)!
                        newData.storyViews = (theStories?.storyViews)!
                        newData.storyImage_path = (theStories?.storyImage_path)!
                        
                        realm.add(newData, update: true)
                        realm.add(newData)
                        try!realm.commitWrite()
                        
                        let story = PIFStories(data: obj)
                        print("OBJ>>> \(obj)")
                        self.stories.append(story!)
                }
                DispatchQueue.main.async {
                    self.requestFStories()
                }
            }
        }
    }
    func requestFStories()
    {
        self.apiRequest?.cancel()
        self.apiRequest = APIRequest.execute(route: APIRouter.featuredStories())
        {
            (data, _, _, error) in
            
            if (error != nil)
            {
                DispatchQueue.main.async
                    {
                }
            }
            else
            {
                let article = data?.value(forKeyPath: "articleEO") as! [AnyObject]
                //debugLog(logMessage: "Results: \(results)")
                self.fStories.removeAll()
                article.forEach
                    {
                        obj in
                        let story = PIFStories(data: obj)
                        self.fStories.append(story!)
                }
                DispatchQueue.main.async {
                    self.checkNumberOfFeatured()
                }
            }
        }
    }
    func checkNumberOfFeatured() {
        if self.fStories.count < 4 {
            var i = 0
            while self.fStories.count < 4 {
                if !self.fStories.contains(self.stories[i]) {
                    self.fStories.append(self.stories[i])
                }
                i = i + 1
            }
        }
        else {
           // debugLog(logMessage: "We are good")
        }
    }
}
