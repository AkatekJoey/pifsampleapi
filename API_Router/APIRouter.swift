//
//  APIRouter.swift
//  PIF
//
//  Created by Louie Je Decatoria on 14/09/2016.
//  Copyright © 2016 Louie Je Decatoria. All rights reserved.
//

import Foundation

public enum APIRouter: URLRequestConvertible {
    
    case getAddress(latitudeLoc: String?, longitudeLoc: String?)
    
    case loi()
    case stories()
    case featuredStories()
    
    case campaigns(latitudeLoc: String?, longitudeLoc: String?)
    case featuredCampaigns(latitudeLoc: String?, longitudeLoc: String?)
    case getCampaigns()
    case getCampaignsByLoi(loiID: String?)
    case getCampaignByID(campaignID: String?)
    
    case streams(userID: String?)

    
    case campaignDonationInfo(request_id: String?, user_currency: String?)
    case payRequest(request_id: String?, access_token: String?, userCurrency: String?, donor_amount: String?, first_name: String?, middle_name: String?, last_name: String?, email_address: String?, contact_number: String?, zipcode: String?, city: String?, state: String?, stateCode: String?, country: String?, countryCode: String?, payment: String?)
    case payRequestBankTransfer(request_id: String?, access_token: String?, userCurrency: String?, donor_amount: String?, first_name: String?, middle_name: String?, last_name: String?, email_address: String?, contact_number: String?, zipcode: String?, city: String?, state: String?, stateCode: String?, country: String?, countryCode: String?, payment: String?, transNo: String?)
    
    case login(email: String?, password: String?, deviceName: String?, deviceModel: String?, versionOS: String?, IPAddress: String?, latitudeLoc: String?, longitudeLoc: String)
    case getProfile(userID: String?)
    
    case regDonorLightDonor(firstName: String?, lastName: String?, email: String?, password: String?, confirm_password: String?, agreed_toc: String?, loi: Int?, role: String?)
    case regDonorLightRequestor(firstName: String?, lastName: String?, email: String?, password: String?, confirm_password: String?, agreed_toc: String?, loi: Int?, role: String?, orgType: String?, orgName: String?)
    case getOrgType()
    
    case regDonorStep1FB(email: String?, role: String?)
    case regDonorStep1Gplus(userName: String?)
    
    case logDonorStep1FB(userName: String?, deviceName: String?, deviceModel: String?, versionOS: String?, IPAddress: String?, latitudeLoc: String?, longitudeLoc: String)
    case logDonorStep1Gplus(userName: String?)
    
    case regDonorStep2(lods: [Int]?, hdykau: Int?, access_token: String?)
    
    
    case regDonorStep3(email: String?, password: String?)
    case regDonorStep4(email: String?, password: String?)
    
    public var URLRequest: NSMutableURLRequest {
        
        var path:String = {
            switch self {
            case .getAddress:
                return "api/public/get-address/$0"
            case .loi:
                return "api/public/loi/"
            case .stories:
                return "api/public/articles/"
            case .featuredStories:
                return "api/public/articles/?featured=1&take=6"
            case .campaigns:
                return "api/public/campaigns/$0"
            case .featuredCampaigns:
                return "api/public/campaigns/$0"
            case .getCampaigns:
                return "api/public/campaign/"
            case .getCampaignsByLoi:
                return "api/public/campaigns/14.5547/121.0244?lod-id=$0"
            case .getCampaignByID:
                return "api/public/campaign/$0"
                
            case .streams:
                return "api/public/load-donor-stream/$0"
               
            case .campaignDonationInfo:
                return "api/public/givenow/$0"
            case .payRequest:
                return "api/public/givenow/cash"
            case .payRequestBankTransfer:
                return "api/public/givenow/cash"
            
            case .login:
                return "api/public/account/login"
            case .logDonorStep1FB:
                return "api/public/account/login/facebook"
            case .logDonorStep1Gplus:
                return "api/public/account/login/google"
                
            case .getProfile:
                return "api/public/profile/$0"
                
            case .regDonorLightDonor:
                return "api/public/register/light"
            case .regDonorLightRequestor:
                return "api/public/register/light"
            case .getOrgType:
                return "api/public/get-orgtypes"
                
            case .regDonorStep1FB:
                return "api/public/register/account/facebook"
            case .regDonorStep1Gplus:
                return "api/public/register/account/google"
                

                
            case .regDonorStep2:
                return "api/public/register/loi"
            case .regDonorStep3:
                return ""
            case .regDonorStep4:
                return ""
            }
        }()
        
        switch self {
        case .getAddress(let latitudeLoc, let longitudeLoc):
            let addrWithLoc = "\((latitudeLoc!))/\((longitudeLoc!))"
            path = path.replacingOccurrences(of: "$0", with: addrWithLoc)
           // debugLog(logMessage: path)
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
            
        case .loi():
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
            
        case .stories():
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
        case .featuredStories():
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
         
        case .campaigns(let latitudeLoc, let longitudeLoc):
            let addrWithLoc = "\((latitudeLoc!))/\((longitudeLoc!))?lod-id=1&all=1"
            path = path.replacingOccurrences(of: "$0", with: addrWithLoc)
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
        case .featuredCampaigns(let latitudeLoc, let longitudeLoc):
            let addrWithLoc = "\((latitudeLoc!))/\((longitudeLoc!))?lod-id=1"
            path = path.replacingOccurrences(of: "$0", with: addrWithLoc)
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
        case .getCampaigns():
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
        case .getCampaignsByLoi(let loiID):
            path = path.replacingOccurrences(of: "$0", with: loiID!)
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
        case .getCampaignByID(let campaignID):
            path = path.replacingOccurrences(of: "$0", with: campaignID!)
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
            
        case .streams(let userID):
            path = path.replacingOccurrences(of: "$0", with: userID!)
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
            
        case .campaignDonationInfo(let request_id, let user_currency):
            let donationByIDandCurrency = "\((request_id!))/\((user_currency!))"
            path = path.replacingOccurrences(of: "$0", with: donationByIDandCurrency)
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
        case .payRequest(let request_id, let access_token, let userCurrency, let donor_amount, let first_name, let middle_name, let last_name, let email_address, let contact_number, let zipcode, let city, let state, let stateCode, let country, let countryCode, let payment):
            let params = ["request_id": request_id!, "access_token": access_token!, "userCurrency": userCurrency!, "donor_amount": donor_amount!, "first_name": first_name!, "middle_name": middle_name!, "last_name": last_name!, "email_address": email_address!, "contact_number": contact_number!, "zipcode": zipcode!, "city": city!, "state": state!, "stateCode": stateCode!, "country": country!, "countryCode": countryCode!, "payment": payment!] as [String : String]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
        case .payRequestBankTransfer(let request_id, let access_token, let userCurrency, let donor_amount, let first_name, let middle_name, let last_name, let email_address, let contact_number, let zipcode, let city, let state, let stateCode, let country, let countryCode, let payment, let transNo):
            let params = ["request_id": request_id!, "access_token": access_token!, "userCurrency": userCurrency!, "donor_amount": donor_amount!, "first_name": first_name!, "middle_name": middle_name!, "last_name": last_name!, "email_address": email_address!, "contact_number": contact_number!, "zipcode": zipcode!, "city": city!, "state": state!, "stateCode": stateCode!, "country": country!, "countryCode": countryCode!, "payment": payment!, "transNo": transNo!] as [String : String]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
            
            
        case .login(let email, let password, let deviceName, let deviceModel, let versionOS, let IPAddress, let latitudeLoc, let longitudeLoc):
            let params = ["username": email!, "password": password!, "deviceName": deviceName!, "deviceModel": deviceModel!, "versionOS": versionOS!, "IPAddress": IPAddress!, "latitudeLoc": latitudeLoc!, "longitudeLoc": longitudeLoc] as [String : String]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
            
        case .getProfile(let userID):
            path = path.replacingOccurrences(of: "$0", with: userID!)
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
            
            
        case .regDonorLightDonor(let firstName, let lastName, let email, let password, let confirm_password, let agreed_toc, let loi, let role):
            let params = ["firstname": firstName! as String, "lastname": lastName! as String, "email": email! as String, "password": password! as String, "confirm_password": confirm_password! as String, "agreed_toc": agreed_toc! as String, "loi": loi! as Int, "role": role! as String] as [String : Any]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
        case .regDonorLightRequestor(let firstName, let lastName, let email, let password, let confirm_password, let agreed_toc, let loi, let role, let orgType, let orgName):
            let params = ["firstname": firstName! as String, "lastname": lastName! as String, "email": email! as String, "password": password! as String, "confirm_password": confirm_password! as String, "agreed_toc": agreed_toc! as String, "loi": loi! as Int, "role": role! as String, "orgType": orgType! as String, "orgName": orgName! as String] as [String : Any]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
        case .getOrgType():
            return ParameterEncoding.encode(path: path, method:HTTPMethod.GET, parameters: nil).0
            
            
        case .regDonorStep1FB(let email, let role):
            let params = ["email": email!, "role": role!] as [String : String]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
        case .regDonorStep1Gplus(let userName):
            let params = ["email": userName!] as [String : String]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
            
        case .logDonorStep1FB(let userName, let deviceName, let deviceModel, let versionOS, let IPAddress, let latitudeLoc, let longitudeLoc):
            let params = ["username": userName!, "deviceName": deviceName!, "deviceModel": deviceModel!, "versionOS": versionOS!, "IPAddress": IPAddress!, "latitudeLoc": latitudeLoc!, "longitudeLoc": longitudeLoc] as [String : String]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
        case .logDonorStep1Gplus(let userName):
            let params = ["username": userName!] as [String : String]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
        
        case .regDonorStep2(let lods, let hdykau, let access_token):
            let params = ["lods": lods! as AnyObject, "hdykau": hdykau! as AnyObject, "access token": access_token! as AnyObject] as [String : AnyObject]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
            
            
            
        case .regDonorStep3(let email, let password):
            let params = ["username": email!, "password": password!] as [String : String]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
        case .regDonorStep4(let email, let password):
            let params = ["username": email!, "password": password!] as [String : String]
            return ParameterEncoding.encode(path: path, method:HTTPMethod.POST, parameters: params as [String : AnyObject]?).0
        }
    }
}

public protocol URLRequestConvertible {
    var URLRequest: NSMutableURLRequest { get }
}

public enum ParameterEncoding {
    
    static func encode(path: String, method: HTTPMethod, parameters: [String : AnyObject]?) -> (NSMutableURLRequest, NSError?) {
        
        var urlPath = ConfigManager.sharedInstance.baseUrl! + path
        //var queryString = ""
        
        if let param = parameters {
            let components = NSURLComponents(string: urlPath)!
            var queryItems: [NSURLQueryItem] = components.queryItems as [NSURLQueryItem]? ?? [NSURLQueryItem]()
            param.forEach { (key, val) in
                queryItems.append(NSURLQueryItem(name: key.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!, value: "\(val)"))
            }
            components.queryItems = queryItems as [URLQueryItem]?
            //queryString = components.query!
            
            if (method == HTTPMethod.GET) {
                urlPath = components.string!
            }
        }
        
        let url = NSURL(string: urlPath)!
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = method.rawValue
        
        if (method == HTTPMethod.POST || method == HTTPMethod.PUT) {
            self.configureBody(request: request, parameters: parameters)
            //self.configureBody(request, query: queryString)
        }
        self.configureHeaders(request: request)
        return (request, nil) //\\//\\//\\//\\
    }
    
    static func configureHeaders(request: NSMutableURLRequest) {
        //application/x-www-form-urlencoded
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        /*
        if let accessToken = GlobalManager.getAccessToken() {
            //request.setValue(accessToken, forHTTPHeaderField: "Authorization")
        }
        */
    }
    
    

    static func configureBody(request: NSMutableURLRequest, parameters: [String : AnyObject]?) {
        var postData: NSData?
        do {
            postData = try JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted) as NSData?
        } catch let err as NSError{
           // debugLog(logMessage: err.description, functionName:"")
        }
        
        //let postData:NSData = query.dataUsingEncoding(NSUTF8StringEncoding)!
        let postLength = "\(postData!.length)"
        request.httpBody = postData! as Data
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
    }
    
    /*
     static func configureBody(request: NSMutableURLRequest, query: String) {
     let postData:NSData = query.dataUsingEncoding(NSUTF8StringEncoding)!
     let postLength = "\(postData.length)"
     request.HTTPBody = postData
     request.setValue(postLength, forHTTPHeaderField: "Content-Length")
     }*/
}

public enum HTTPMethod : String {
    case OPTIONS
    case GET
    case HEAD
    case POST
    case PUT
    case PATCH
    case DELETE
    case TRACE
    case CONNECT
}

let PERSISTENCE_FILENAME = "PersistedData.data"
let PERSISTENCE_ROOT_INDEX = "root_index"

class PersistenceManager: NSObject {
    
    var isActive: Bool = true
    
    //MARK: Shared Instance
    static let sharedInstance : PersistenceManager = {
        let instance = PersistenceManager()
        return instance
    }()
    
    func loadDeserializedObjectForKey(key: String!) -> NSObject? {
        let data = self.loadData()
        return data.object(forKey: key) as? NSObject
    }
    
    
    func loadData() -> NSMutableDictionary {
        let path = self.filePath()
        let root = NSKeyedUnarchiver.unarchiveObject(withFile: path!) as! NSMutableDictionary
        let data = root.object(forKey: PERSISTENCE_ROOT_INDEX) as! NSMutableDictionary
        return data
    }
    
    func filePath() -> String! {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let path = documentsPath as NSString
    
        return path.appendingPathComponent(PERSISTENCE_FILENAME)
    }
}


class GlobalManager: NSObject {
    static let sharedInstance : GlobalManager = {
        let instance = GlobalManager()
        return instance
    }()
    func getAccessToken() -> String? {
        guard let accessToken = PersistenceManager.sharedInstance.loadDeserializedObjectForKey(key: "access-token") else {
            return nil
        }
        return accessToken as? String
    }
}
