//
//  RealmStoriesObject.swift
//  pifSampleApi
//
//  Created by Administrator on 21/04/2017.
//  Copyright © 2017 Akatek Solutions Inc. All rights reserved.
//

import UIKit
import Foundation

class RealmStoriesObject {
    dynamic var storyId: String = ""
    dynamic var storyTitle: String = ""
    dynamic var storySummary: String = ""
    dynamic var storyDesc: String = ""
    dynamic var storyImage: String = ""
    dynamic var storyVideo: String = ""
    dynamic var storyCity: String = ""
    dynamic var storyCountry: String = ""
    dynamic var storyDate: String = ""
    dynamic var storyLOI: String = ""
    dynamic var storySlug: String = ""
    dynamic var storyViews: String = ""
    dynamic var storyImage_path: String = ""
    
    init(storyId: String, storyTitle: String, storySummary: String, storyDesc:String, storyImage: String, storyVideo:String,
         storyCity:String, storyCountry:String, storyDate: String, storyLOI:String, storySlug:String, storyViews:String,
         storyImage_path:String){
        
        self.storyId = storyId
        self.storyTitle = storyTitle
        self.storySummary = storySummary
        self.storyDesc = storyDesc
        self.storyImage = storyImage
        self.storyVideo = storyVideo
        self.storyCity = storyCity
        self.storyCountry = storyCountry
        self.storyDate = storyDate
        self.storyLOI = storyLOI
        self.storySlug = storySlug
        self.storyViews = storyViews
        self.storyImage_path = storyImage_path
    }
    
    
}
