//
//  realmrrealmStories.swift
//  pifSampleApi
//
//  Created by Administrator on 25/04/2017.
//  Copyright © 2017 Akatek Solutions Inc. All rights reserved.
//

import UIKit

class realmrrealmStories: NSObject {
 
    var storyId: String?
    var storyTitle: String?
    var storySummary: String?
    var storyDesc: String?
    var storyImage: String?
    var storyVideo: String?
    var storyCity: String?
    var storyCountry: String?
    var storyDate: String?
    var storyLOI: String?
    var storySlug: String?
    
    var storyViews: String?
    var storyImage_path: String?
    
    override init() {
        self.storyId = ""
        self.storyTitle = ""
        self.storySummary = ""
        self.storyDesc = ""
        self.storyImage = ""
        self.storyVideo = ""
        self.storyCity = ""
        self.storyCountry = ""
        self.storyDate = ""
        self.storyLOI = ""
        self.storySlug = ""
        
        self.storyViews = ""
        self.storyImage_path = ""
    }
    
    required init?(data: AnyObject) {
        super.init()
        
        var key = "id"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyId = data.value(forKeyPath: key) as? String
        }
        
        
        key = "Title"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyTitle = data.value(forKeyPath: key) as? String
        }
        
        key = "summary"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storySummary = data.value(forKeyPath: key) as? String
        }
        
        key = "content"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyDesc = data.value(forKeyPath: key) as? String
        }
        
        key = "Preview_Image"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyImage = data.value(forKeyPath: key) as? String
        }
        
        key = "Video_Url"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyVideo = data.value(forKeyPath: key) as? String
        }
        
        key = "city"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyCity = data.value(forKeyPath: key) as? String
        }
        
        key = "country"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyCountry = data.value(forKeyPath: key) as? String
        }
        
        key = "created_at"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyDate = data.value(forKeyPath: key) as? String
        }
        
        key = "prop_l_o_d"
        if (isValidKeypath(data: data, keypath: key)) {
            let storyLOI = data.value(forKeyPath: key) as? [AnyObject]
            let loi = storyLOI![0]
            
            if (isValidKeypath(data: loi, keypath: "line_of_donation")) {
                self.storyLOI = loi.value(forKeyPath: "line_of_donation") as? String
            }
            //self.storyLOI = data.value(forKeyPath: key) as? String
        }
        
        key = "slug"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storySlug = data.value(forKeyPath: key) as? String
        }
        
        key = "views"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyViews = data.value(forKeyPath: key) as? String
        }
        
        key = "image_path"
        if (isValidKeypath(data: data, keypath: key)) {
            self.storyImage_path = data.value(forKeyPath: key) as? String
        }
    }

    
}
