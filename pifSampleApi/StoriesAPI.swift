
//
//  StoriesAPI.swift
//  pifSampleApi
//
//  Created by Administrator on 25/04/2017.
//  Copyright © 2017 Akatek Solutions Inc. All rights reserved.
//

import Foundation
import RealmSwift
import DateHelper


typealias JSONObject = [String: AnyObject]

func delay(seconds: UInt32, completion:@escaping ()->Void) {
    let popTime = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * Double(seconds) )) / Double(NSEC_PER_SEC)
    
    DispatchQueue.main.asyncAfter(deadline: popTime) {
        completion()
    }
}

class StoriesAPI {

    let repoUrl = URL(string: "http://api.dev.passitforward.org/api/public/articles/?featured=1&take=6")!
   
    func startFetching() {
        DispatchQueue.global(qos: .background).async {[weak self] in
            guard let this = self else { return }
            
            URLSession.shared.dataTask(with: this.repoUrl, completionHandler: { data, response, error in
                print("updated")
                if let data = data, let json = try! JSONSerialization.jsonObject(with: data, options: []) as? Array<JSONObject> {
                    this.persistRepos(json)
                }
                delay(seconds: 10, completion: this.startFetching)
            }).resume()
        }
    }

    func persistRepos(_ json: Array<[String: AnyObject]>) {
        let realm = try! Realm()
        try! realm.write {
            for jsonRepo in json {
                guard let id = jsonRepo["id"] as? Int,
                    let stars = jsonRepo["stargazers_count"] as? Int,
                    let name = jsonRepo["name"] as? String,
                    let pushedAt = jsonRepo["pushed_at"] as? String else {
                        break
                }
                
                if let repo = realm.object(ofType: RealmFeaturedStories.self, forPrimaryKey: id) {
                    //update
                    let lastPushDate = Date(fromString: pushedAt, format: .iso8601(.DateTimeSec))
                    if repo.pushedAt.distance(to: lastPushDate.timeIntervalSinceReferenceDate) > 1e-16 {
                        repo.pushedAt = lastPushDate.timeIntervalSinceReferenceDate
                    }
                    if repo.stars != stars {
                        repo.stars = stars
                    }
                } else {
                    //insert
                    let repo = Repo()
                    repo.name = name
                    repo.stars = stars
                    repo.id = id
                    repo.pushedAt = Date(fromString: pushedAt, format: .iso8601(.DateTimeSec)).timeIntervalSinceReferenceDate
                    realm.add(repo)
                }
            }
        }
    }
    
}
