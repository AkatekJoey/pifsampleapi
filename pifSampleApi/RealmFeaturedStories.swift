//
//  RealmFeaturedStories.swift
//  pifSampleApi
//
//  Created by Administrator on 25/04/2017.
//  Copyright © 2017 Akatek Solutions Inc. All rights reserved.
//

import UIKit
import RealmSwift

class RealmFeaturedStories: Object{

    dynamic var storyId: String = ""
    dynamic var storyTitle: String = ""
    dynamic var storySummary: String = ""
    dynamic var storyDesc: String = ""
    dynamic var storyImage: String = ""
    dynamic var storyVideo: String = ""
    dynamic var storyCity: String = ""
    dynamic var storyCountry: String = ""
    dynamic var storyDate: String = ""
    dynamic var storyLOI: String = ""
    dynamic var storySlug: String = ""
    dynamic var storyViews: String = ""
    dynamic var storyImage_path: String = ""
    
    override class func primaryKey() -> String{
        return "storyId"
    }
}
