//
//  ViewController.swift
//  pifSampleApi
//
//  Created by Administrator on 20/04/2017.
//  Copyright © 2017 Akatek Solutions Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

 
        self.getStories()
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let path = documentsPath as NSString
        
        
        print("path for file is \(path)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getStories(){
        _ = StoriesManager.sharedInstance.addStories()
    }
}




class ConfigManager: NSObject {
    var baseUrl: String?
    deinit {
        // perform the deinitialization
        NotificationCenter.default.removeObserver(self)
    }
    //MARK: Shared Instance
    static let sharedInstance : ConfigManager = {
        let instance = ConfigManager()
        instance.configure()
        return instance
    }()
    
    func configure() {
        var myDict: NSDictionary?
        if let path = Bundle.main.path(forResource: "Config", ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        }
        
        if let dict = myDict {
            self.baseUrl = dict["BASE_URL"] as? String
        }
    }
}
